import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.stream.Stream;

public class Server {

    private ServerSocket serverSocket;
    private Socket client;
    private DataInputStream texto;

    public Server(Integer port) {
        try {

            this.serverSocket = new ServerSocket(port);
            System.out.println("Server is running in port :" + serverSocket.getLocalPort());
            this.client = serverSocket.accept();
            this.texto =
                    new DataInputStream(
                            new BufferedInputStream(
                                    client.getInputStream()));
            String mensaje = "";
            DataOutputStream respuesta = new DataOutputStream(this.client.getOutputStream());
            while(mensaje.compareTo("conexion finalizada") != 0){
                mensaje = texto.readUTF();
                System.out.println(mensaje);
                respuesta.writeUTF("Mensaje Recibido");
            }
            this.serverSocket.close();
            this.client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*public static void main(String[] args) {
        try {

            serverSocket = new ServerSocket(8000);
            System.out.println("Server is running in port :" + serverSocket.getLocalPort());
            client = serverSocket.accept();
            DataInputStream texto =
                    new DataInputStream(
                            new BufferedInputStream(
                                    client.getInputStream()));
            String mensaje = "";
            while(mensaje.compareTo("conexion finalizada") != 0){
                mensaje = texto.readUTF();
                System.out.println(mensaje);
            }
            serverSocket.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}
