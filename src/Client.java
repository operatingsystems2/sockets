import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket client;
    private DataOutputStream texto;
    //private String ip;
    //private Integer port;

    public Client(String ip, Integer port) {
        try {
            this.client = new Socket(ip, port);
            this.texto = new DataOutputStream(client.getOutputStream());
            DataInputStream recibido = new DataInputStream(new BufferedInputStream(this.client.getInputStream()));
            String mensaje = "";
            Scanner scanner = new Scanner(System.in);
            while (mensaje.compareTo("conexion finalizada") != 0){
                mensaje = scanner.nextLine();
                this.texto.writeUTF(mensaje);
                String respuesta = recibido.readUTF();
                System.out.println(respuesta);
            }
            this.texto.close();
            this.client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public static void main(String[] args) {
        try {
            Socket client = new Socket("10.10.214.121", 8000);
            DataOutputStream texto = new DataOutputStream(client.getOutputStream());
            String mensaje = "";
            Scanner scanner = new Scanner(System.in);
            while (mensaje.compareTo("conexion finalizada") != 0){
                mensaje = scanner.nextLine();
                texto.writeUTF(mensaje);
            }
            texto.close();
            client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
